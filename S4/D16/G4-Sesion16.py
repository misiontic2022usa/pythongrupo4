""" Programa para hacer las operaciones basicas con dos numeros 
"""

import math


def suma(numero1: int, numero2: int):
    """Funcion suma, suma dos numeros

    Args:
        numero1 (int): Numero entero
        numero2 (int): Numero entero

    Returns:
        int: Numero entero
    """
    resultado = numero1 + numero2
    return resultado

def resta(numero1: int, numero2: int):
    """Resta dos numeros

    Args:
        numero1 (int): Numero entero
        numero2 (int): Numero entero

    Returns:
        int : Numero entero
    """
    resultado = numero1 - numero2
    return resultado

def multiplicacion(numero1: int, numero2: int):
    """Multiplicación: multiplica dos numeros

    Args:
        numero1 (int): Numero entero
        numero2 (int): Numero entero

    Returns:
        int : Numero entero
    """
    resultado = numero1 * numero2
    return resultado

def division(numero1 : int, numero2: int):
    """Division de dos numeros

    Args:
        numero1 (int): Numero entero
        numero2 (int): Numero entero

    Returns:
        float:  si numero2 es diferente de 0 hace la division,
                en caso contrario regresa -1
    """
    if numero2 != 0:
        resultado = numero1/numero2
    else:
        resultado = -1
    return resultado

def menu():
    """Menu muestra un menu de opciones

    Returns:
        str: La opción seleccionada
    """
    print("Seleccione un operacion ")
    print("(+) Suma")
    print("(-) Resta")
    print("(*) Multiplicación")
    print("(/) División")
    print("(T) Terminar")
    opcion = input(">_ ")
    return opcion

def calculadora():
    """Programa que hace de calculadora
    """
    a = int(input("Ingrese un numero "))
    b = int(input("Ingrese otro numero "))
    terminar = False
    while not terminar:
        opcion = menu()
        if opcion == "+":
            total = suma(a,b)
            print(total)
        elif opcion == "-":
            total = resta(a,b)
            print(total)
        elif opcion == "*":
            total = multiplicacion(a,b)
            print(total)
        elif opcion == "/":
            total = division(a,b)
            print(total)
        elif opcion.upper() == "T":
            terminar = True
        else:
            print("opcion no valida")

'''Crea un programa que pida al usuario un número real y muestra su raíz cuarta 
(la raíz cuadrada de la raíz cuadrada)
'''

def raizCuadrada(numero: float):
    """raizCuadrada calcula la raiz cuadrada de un numero elevando a 1/2

    Args:
        numero (float): numero al que queremos calcular la raiz cuadrada 

    Returns:
        float: numero
    """
    resultado = numero**(1/2)
    return resultado

#calculadora()
def raizCuarta():
    """Crea un programa que pida al usuario un número real y muestra su raíz cuarta 
        (la raíz cuadrada de la raíz cuadrada)
    """
    numero = float(input("Ingrese un numero "))
    total = raizCuadrada(numero)
    print(total)
    total = raizCuadrada(total)
    print(total)


"""Crear un programa que calcule y escriba el área para un circulo.
"""
def areaCirculo(radio: float):
    """Calcula el area de un circulo

    Args:
        radio (float): numero flotante

    Returns:
        float: valor del area
    """
    resultado = radio * pow(math.pi,2) 
    return resultado

def circulo():
    """Crear un programa que calcule y escriba el área para un circulo.
    """
    medioDiametro = float(input("Ingrese el radio "))
    total = areaCirculo(medioDiametro)
    print(total)
