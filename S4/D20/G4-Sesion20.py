'''Enunciado
Detección de fallas en una línea de producción.

En una fabrica de baldosas de cerámica se producen por día una gran cantidad de este producto. 
Es importante hacer control de calidad sobre el producto. Este control consiste en revisar si en 
un lote de N baldosas hay baldosas defectuosas (puede variar la textura o el color). La solicitud 
del gerente de la planta es que se construya un programa en Python que pueda detectar la cantidad 
de baldosas defectuosas en una de las líneas de producción de la fabrica.

Para detectar si una baldosa es diferente a otra, un sensor escanea las baldosas y si hay alguna 
diferencia, guarda un registro en la memoria. La memoria del sensor esta limitada por la cantidad 
de baldosas que se producen en un intervalo de tiempo determinado.

Entrada	La entrada estará formada por dos líneas:
La primera línea aparecerá dos números N y K que indican el número de baldosas a revisar y el 
número de baldosas que el sensor es capaz de guardar (1≤N≤1000,1≤K≤1000)
La segunda línea contiene M números (entre 1 y 100) separados por espacios que representan las 
baldosas revisadas por el sensor
Las baldosas se consideran defectuosas si están representados por el mismo número

Salida	El programa imprimirá tres números separados por un espacio.

El primero representará el número total de fallas detectadas
El segundo representará la cantidad de fallas detectadas por el sensor considerando que al revisar 
una baldosa solo es capaz de guardar las K baldosas anteriores
El tercero representa la cantidad de baldosas revisadas por el sensor


Analisis:
            Detectar baldosas defectuosas

Entrada: dos líneas, La primera N K
            N ---> numero de baldosas a revisar 1≤N≤1000
            K ---> la memoria, cantidad de baldosas a revisar antes de 1≤K≤1000
        la segunda linea M numeros entre 1 y 100

Salida: una linea con tres valores 
            totalFallas     --->    cantidad de numeros repetidos en la segunda linea de entrada M
            fallasDetectadas   --->    El numero de repetidos que tenemos en los K anteriores
            cantBalRevisadas -->    cantidad de baldosas revisadas

Análisis:   funcion repetidos(lista) ---> CantidadRepetidos
                unicos = []
                longitud = len(lista)
                Para cada Baldosa en la lista:
                    Si not(Baldosa esta en unicos) entonces
                        unicos.agregar(Baldosa)
                    FinSi
                finPara
                CantidadRepetidos = longitud - len(unicos)
                devolver CantidadRepetidos

            funcion detectarMemoria(lista, cantidad) ---> Fallas
                longitud = len(lista)
                actual = 1
                Fallas = 0
                Mientras actual < longitud haga
                    inicio = 0
                    Si actual > K entonces
                        inicio = Actual - K
                    FinSi
                    memoria = lista[inicio:actual]
                    Si lista[actual] esta en memoria Entonces
                        Fallas = Fallas + 1
                    finSi
                    Para BaldMemoria en la memoria:
                        Si BalMemoria==lista[actual] Entonces
                            Fallas = Fallas + 1
                            break
                    FinPara
                    actual =  actual + 1
                FinMientras
                devolver Fallas

            
            leer linea1.split()
            leer linea2.split()
            N = linea1[0]
            K = linea1[1]
            M = linea2
            cantBalRevisadas = len(M)
            totalFallas =  repetidos(M)
            fallasDetectadas = detectarMemoria(M,K)




            





Casos de prueba:

Entrada	Salida Esperada
5 1
1 2 3 1 2	2 0 5
5 2
1 2 3 1 2	2 0 5
5 3
1 2 3 1 2	2 2 5
Dificultad
Instrucciones
Instrucciones para la calificación automática


Cada línea debe contener los valores de los parámetros separados por un espacio.
Es importante no utilizar ningún mensaje a la hora de capturar las entradas, es decir, al utilizar la función input()no agregue ningún texto para capturar los datos.
El resultado "NO DISPONIBLE" siempre debe imprimirse en mayúscula.

5 1
1 2 3 1 2	2 0 5

funcion repetidos([1 2 3 1 2]) ---> CantidadRepetidos
    unicos = []
    longitud = len([1 2 3 1 2]) = 5
    Para cada Baldosa en la [1 2 3 1 2]:
        Si not(1 esta en []) entonces
            unicos.agregar(1) ---> [1]
        FinSi
        Si not(2 esta en [1]) entonces
            unicos.agregar(2) ---> [1 2]
        FinSi
        Si not(3 esta en [1 2]) entonces
            unicos.agregar(3) ---> [1 2 3]
        FinSi
        Si not(1 esta en [1 2 3]) entonces
        FinSi
        Si not(2 esta en [1 2 3]) entonces
        FinSi
    finPara
    CantidadRepetidos = 5 - len([1 2 3]) ---> 5-3  = 2 
    devolver 2

funcion detectarMemoria([1 2 3 1 2], 1) ---> Fallas
    longitud = len([1 2 3 1 2]) = 5
    actual = 1
    Fallas = 0
    Mientras 1 < 5 haga
        inicio = 0
        Si 1 > 1 entonces
        FinSi
        memoria = lista[0:1] --->[1]
        Para BaldMemoria en la [1]:
            Si 1==2 Entonces
            FinSi
        FinPara
        actual =  1 + 1  --->2
        inicio = 0
        Si 2 > 1 entonces
            inicio = 2 - 1  ---> 1
        FinSi
        memoria = lista[1:2]  ---> [2]
        Para BaldMemoria en la [2]:
            Si 2==3 Entonces
            FinSi
        FinPara
        actual =  2 + 1   --->3
        inicio = 0
        Si 3 > 1 entonces
            inicio = 3 - 1  --->2
        FinSi
        memoria = lista[2:3]  --->[3]
        Para BaldMemoria en la [3]:
            Si 3==1 Entonces
            FinSi
        FinPara
        actual =  3 + 1  --> 4
        inicio = 0
        Si 4 > 1 entonces
            inicio = 4 - 1  ---> 3
        FinSi
        memoria = lista[3:4]  --->[1]
        Para BaldMemoria en la [1]:
            Si 1==2 Entonces
            FinSi
        FinPara
        actual =  4 + 1  --->5
    FinMientras
    devolver 0


leer [5 1].split()
leer [1 2 3 1 2].split()
N = 5
K = 1
M = [1 2 3 1 2]
totalFallas =  repetidos([1 2 3 1 2]) = 2
fallasDetectadas = detectarMemoria([1 2 3 1 2],1)  =  0
cantBalRevisadas = len([1 2 3 1 2]) =  5




funcion repetidos([1 2 3 1 2]) ---> CantidadRepetidos
    unicos = []
    longitud = len([1 2 3 1 2]) = 5
    Para cada Baldosa en la [1 2 3 1 2]:
        Si not(1 esta en []) entonces
            unicos.agregar(1) ---> [1]
        FinSi
        Si not(2 esta en [1]) entonces
            unicos.agregar(2) ---> [1 2]
        FinSi
        Si not(3 esta en [1 2]) entonces
            unicos.agregar(3) ---> [1 2 3]
        FinSi
        Si not(1 esta en [1 2 3]) entonces
        FinSi
        Si not(2 esta en [1 2 3]) entonces
        FinSi
    finPara
    CantidadRepetidos = 5 - len([1 2 3]) ---> 5-3  = 2 
    devolver 2

funcion detectar([1 2 3 1 2], 3) ---> Fallas
    longitud = len([1 2 3 1 2])  ---> 5
    actual = 1
    Fallas=0
    Mientras 1 < 5 haga
        inicio = 0
        Si 1 > 3 entonces
        FinSi
        memoria = lista[0:1]   [1]
        Para BaldMemoria en la [1]:
            Si 1==2 Entonces
            Finsi
        FinPara
        actual =  1 + 1   2
        inicio = 0
        Si 2 > 3 entonces
        FinSi
        memoria = lista[0:2] [1 2]
        Para BaldMemoria en la [1 2]:
            Si 1==3 Entonces
            finSi
            Si 2==3 Entonces
            FinSi
        FinPara
        actual =  2 + 1    3
        inicio = 0
        Si 3 > 3 entonces
        FinSi
        memoria = lista[0:3]   ---> [1 2 3]
        Para BaldMemoria en la [1 2 3]:
            Si 1==1 Entonces
                Fallas = 0 + 1
            FinSi
            Si 2==1 Entonces
            FinSi
            Si 3==1 Entonces
            FinSi
        FinPara
        actual =  3 + 1    4
        inicio = 0
        Si 4 > 3 entonces
            inicio = 4 - 3    1
        FinSi
        memoria = lista[1:4]  [2 3 1]
        Para BaldMemoria en la [2 3 1]:
            Si 2==2 Entonces
                Fallas = 1 + 1     2
            FinSi
            Si 3==2 Entonces
            FinSi
            Si 1==2 Entonces
            FinSi
        FinPara
        actual =  4 + 1   5
    FinMientras
    devolver 2


leer [5 3].split()
leer [1 2 3 1 2].split()
N = 5
K = 3
M = [1 2 3 1 2]
totalFallas =  repetidos([1 2 3 1 2])       = 2
fallasDetectadas = detectar([1 2 3 1 2],3)  = 2
cantBalRevisadas = len([1 2 3 1 2])         = 5


7 2
1 1 1 1 1 1 1
detectar =11, detectar =6
'''




def detectarMemoria(lista, cantidad):
    longitud = len(lista)
    actual = 1
    Fallas = 0
    while actual < longitud:
        inicio = 0
        if actual > cantidad :
            inicio = actual - cantidad
        memoria = lista[inicio:actual]
        if lista[actual] in memoria :
            Fallas = Fallas + 1
        '''
        for BaldMemoria in memoria:
            if BaldMemoria==lista[actual] :
                Fallas = Fallas + 1
                break'''
        actual =  actual + 1
    return Fallas

print(detectarMemoria([1,1,1,1,1,1,1],2))