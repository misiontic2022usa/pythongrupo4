'''
Crear un programa en python donde se va a declarar un diccionario para guardar 
los precios de distintas frutas. El programa pedirá el nombre de la fruta y 
la cantidad que se ha vendido y mostrará el precio final de la fruta a partir 
de los datos guardados en el diccionario. Si la fruta no existe nos dará un error. 
Tras cada consulta el programa nos preguntará si queremos hacer otra consulta.
'''

precios =  dict()

def guardarPrecio( precio: int, fruta :  str):
    precios[fruta.upper()] = precio

def existeFruta(fruta: str):
    existe = True
    if precios.get(fruta.upper())==None:
        existe = False
    return existe

def menu():
    print('Seleccione una opción (A,V,S)')
    print('(A)gregar una fruta')
    print('(V)ender una fruta')
    print('(S)alir del programa')
    opcion = input(">_").upper()
    return opcion

def vender(fruta : str, Cantidad :int):
    resultado = precios[fruta.upper()] * Cantidad
    return resultado

def opciones( opcion : str):
    if opcion =="A":
        fruta = input('Ingrese la fruta ')
        precio = float(input(f'Ingrese el precio unitario para {fruta} '))
        guardarPrecio( precio, fruta )
    elif opcion =="V":
        fruta = input('Ingrese la fruta ')
        cantidad = int(input(f'Ingrese la cantidad de {fruta} a vender '))
        if existeFruta(fruta):
            total = vender(fruta, cantidad)
            print(f'el precio para {cantidad} de {fruta} es ${total}')
        else:
            print(f"La fruta {fruta} no existe en el diccionario")


opcion = "E"
while opcion != "S":
    opcion = menu()
    opciones(opcion)