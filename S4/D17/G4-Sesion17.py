# diccionario = {clave:valor, clave:valor} las claves son diferentes, 
# los valorse pueden ser iguales
estudiantes = {1:'Carlos', 2:'Ana', 3:'Juan',4:'Lucia', 5:'Juan'}
nuevos = {7:'Maria', 6:'Rosa',9:'Gabriel'}

print(estudiantes[3])

estudiantes.update(nuevos)

del(nuevos)

print(estudiantes)

print(estudiantes.get(3))

listaColores = ["Azul", "Rojo","Verde", "Amarillo", "Blanco", "Negro"]
print(listaColores)
diccColores = dict()
for color in listaColores:
    diccColores[color] = color

print(diccColores)


'''
Escriba una función en python que pida un número entero positivo y 
que cree un diccionario cuyas claves sean desde 
el número 1 hasta el número indicado, y los valores sean los 
cuadrados de las claves.'''
    
def cuadrado(diccionario : dict,  numero : int):
    for i in range(numero):
        diccionario[i+1] = (i+1)*(i+1)
    return diccionario

'''
Escribe una función que reciba como parámetro una cadena y devuelva un diccionario con 
la cantidad de apariciones de cada carácter en la cadena.'''
def caracteres(cadena: str):
    dicCaracteres = dict()
    for caracter in cadena:
        dicCaracteres[caracter.lower()]= cadena.lower().count(caracter.lower())
    return dicCaracteres

'''
Crear un programa en python donde se va a declarar un diccionario para guardar 
los precios de distintas frutas. El programa pedirá el nombre de la fruta y 
la cantidad que se ha vendido y mostrará el precio final de la fruta a partir 
de los datos guardados en el diccionario. Si la fruta no existe nos dará un error. 
Tras cada consulta el programa nos preguntará si queremos hacer otra consulta.
'''

cuadrados = dict()
numero =  int(input("Ingrese un numero para crear un diccionario "))
cuadrado(cuadrados,numero)
print(cuadrados)

cadena = input("ingrese un texto \n")
dcaracteres = caracteres(cadena)
print(dcaracteres)
