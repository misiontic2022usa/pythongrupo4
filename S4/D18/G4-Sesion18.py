"""Juego del triqui
 Tablero
        [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]]
Jugadores 2 --> X y O, por turno 
Gana 1=2=3, 4=5=6, 7=8=9, 1=4=7, 2=5=8, 3=6=9, 1=5=9, 3=5=7
Empate --> si no hay ganador
Termina, si no hay ganador o si no quedan espacios
"""
def crearTablero(tablero: list):
    """Crea un tablero vacio

    Args:
        tablero (list): todas las 9 casillas estan en blanco
    """
    for j in range(3):
        fila=[]
        for i in range(3):
            fila.append(' ')
        tablero.append(fila)

def imprimirTablero(tablero: list):
    """me imprime el estado actual del tablero, si la posicion no tiene X o O,
    entonces imprime , la posicion de 1 - 9 

    Args:
        tablero (list): todas las 9 casillas 
    """
    posicion = 1
    for fila in tablero:
        for columna in fila:
            if columna == " ":
                print(posicion, end = " " )
            else:
                print(columna, end = " " )
            posicion += 1
        print(" ")

def leerJugada(tablero: list, jugador: str):
    """muestra el tablero y la opcion de cual sera la siguiente jugada

    Args:
        tablero (list): el tablero de juego
        jugador (str): debe ser X o O
    """
    imprimirTablero(tablero)
    posicion= int(input("Elije la posicion 0-9, que no tenga X o O "))
    if posicion == 1:
        tablero[0][0]= jugador
    elif posicion == 2:
        tablero[0][1]= jugador 
    elif posicion == 3:
        tablero[0][2]= jugador 
    elif posicion == 4:
        tablero[1][0]= jugador 
    elif posicion == 5:
        tablero[1][1]= jugador 
    elif posicion == 6:
        tablero[1][2]= jugador 
    elif posicion == 7:
        tablero[2][0]= jugador 
    elif posicion == 8:
        tablero[2][1]= jugador 
    elif posicion == 9:
        tablero[2][2]= jugador 


def hayGanador(tablero: list):
    """Verifica si tenemos un ganador del triqui

    Args:
        tablero (list):  el tablero de juego

    Returns:
        Bool: devuelve verdadero si hay ganador, falso en caso contrario
    """
    #1=2=3, 4=5=6, 7=8=9, 1=4=7, 2=5=8, 3=6=9, 1=5=9, 3=5=7
    # 1=2=3 ---> 1==2 y 1==3 o 1==2 y 2==3
    ganador = False
    if (((tablero[0][0] == tablero[0][1] and tablero[0][0] == tablero[0][2]) and tablero[0][0]!= " ") or(
        (tablero[1][0] == tablero[1][1] and tablero[1][0] == tablero[1][2] )and tablero[1][0]!= " " )or (
        (tablero[2][0] == tablero[2][1] and tablero[2][0] == tablero[2][2] )and tablero[2][0]!= " " )or(
        (tablero[0][0] == tablero[1][0] and tablero[0][0] == tablero[2][0] )and tablero[0][0]!= " " )or(
        (tablero[0][1] == tablero[1][1] and tablero[0][1] == tablero[2][1] )and tablero[0][1]!= " " )or(
        (tablero[0][2] == tablero[1][2] and tablero[0][2] == tablero[2][2] )and tablero[0][2]!= " " )or(
        (tablero[0][0] == tablero[1][1] and tablero[0][0] == tablero[2][2] )and tablero[0][0]!= " " )or(
        (tablero[0][2] == tablero[1][1] and tablero[0][2] == tablero[2][0] )and tablero[0][2]!= " ")):
        ganador = True
    return ganador

def quedanJugadas(tablero: list):
    """indica si quedan jugadas por hacer

    Args:
        tablero (list):  el tablero de juego

    Returns:
        Bool : verdaderosi hay espacios por jugar, falso en caso contrario
    """
    quedan = False
    for fila in tablero:
        for columna in fila:
            if columna == " ":
                quedan = True
                break
    return quedan

def cambiarJugador(jugador: str):
    if jugador =="X":
        return "O"
    else:
        return "X"


mitablero=[]
crearTablero(mitablero)
jugador="X"
while not(hayGanador(mitablero)) and quedanJugadas(mitablero):

    leerJugada(mitablero,jugador)

    if hayGanador(mitablero):
        imprimirTablero(mitablero)
        print("Muy bien ganaste")
    jugador = cambiarJugador(jugador)
if not(quedanJugadas):
    print("Empate")