import numpy as np
miArreglo = np.array([1,2,3,4,5,6,7,8,9,0])
print(miArreglo)
print(type(miArreglo))
print(len(miArreglo))
print(miArreglo[5])
print(miArreglo[3]+miArreglo[6])
miArreglo[8] = 10
print(miArreglo)
print(miArreglo[1:8])
print(miArreglo[1:8:2])
print(miArreglo[:6])
print(miArreglo[6:])
print(miArreglo[::-1])
misFrutas = np.array(['Manzana','Limon','Pera','Naranja'])
print(misFrutas.dtype)
print(miArreglo.dtype)
arreglo2 = np.array([1,2,3,4,],dtype="S")
print(arreglo2)
print(arreglo2.dtype)

A = np.array([[1,2],[3,4]])
print(A.dtype)
print(A.shape)
B =  np.array([[8,7],[6,5]])
print(B.dtype)
print(B.shape)
C = A+B
print(C)
print(C.shape)
print(C.dtype)

A = np.array([[2,4,6],[1,3,5]])
B = np.array([[5,6],[1,4],[7,3]])
C = A.dot(B) 
print(A)
print(A.shape)
print(B)
print(B.shape)
print(C)
print(A.transpose().shape)
print(A[1])
print(A[:,1])

'''
Cree un objeto ndarray unidimensional con una longitud de 10 y todos 0, y luego haga que el quinto elemento sea igual a 1.
'''
ejercicio1 =  np.array([0,0,0,0,0,0,0,0,0,0])
print(ejercicio1)
ejercicio1[5] = 1
print(ejercicio1)
'''
Cree una matriz bidimensional, use el índice para obtener los datos en la 
segunda fila, primera columna y tercera fila, segunda columna.
'''
ejercicio2 = np.array([[1,2,3],[4,5,6],[7,8,9]])
print(ejercicio2)
print(ejercicio2.shape)
print(ejercicio2[1])
print(ejercicio2[:,0])
print(ejercicio2[2])
print(ejercicio2[:,1])
K = np.array([[[1,2],[3,4]],[[5,6],[7,8]]])
print(K)
print(K.shape)
print(K[0])
print(K[:,0])
print(K[1][1][0])
'''Crear una matriz de identidad de 3x3'''
ejercicio3 = np.identity(3)
print(ejercicio3)
print(ejercicio3.shape)
'''
Utilizar numpy para generar un arreglo de 25 números aleatorios con una 
distribución normal'''
ejercicio4 = np.random.default_rng()
print(ejercicio4.random((25,)))