'''
Fibonacci: 0,1,1,2,3,5,8,13,21,34,55,89

fib(n) =  fib(n-1)+ fib(n-2)
fib(7) =    fib(6)  +   fib(5)
            fib(6) =    fib(5) +    fib(4)
            fib(5) =    fib(4) +    fib(3)
            fib(4) =    fib(3) +    fib(2)
            fib(3) =    fib(2)  +   fib(1)
            fib(2) =    fib(1) +    fib(0)

fib(1) = 1
fib(0) = 0
'''
anterior = 0
actual = 1
print(anterior)
print(actual)
for i in range(1,1000):
  fibonacci = anterior + actual
  anterior = actual
  actual = fibonacci
print(fibonacci)

def fibonacci(numero):
    if numero>2:
        return fibonacci(numero-1)+fibonacci(numero-2)
    else:
        return 1

print("fibonacci reducido ",fibonacci(10))


def cuentaAtras(n):
    print(n)
    if n>0:
        cuentaAtras(n-1)
    else:
        print("despego")

cuentaAtras(10)

def factorial(n):
    if n>1:
        return n * factorial(n-1)
    else:
        return 1

print(factorial(4))

setup_string ='''
def factorial(n):
    if n>1:
        return n * factorial(n-1)
    else:
        return 1'''

from timeit import timeit

print(timeit('factorial(100)', setup=setup_string, number=1000))

'''Calcular suma de lista'''

def sumalista(lista):
    if len(lista)>1:
        actual = lista.pop()
        return int(actual)+sumalista(lista)
    else:
        return int(lista[0])


mylista=[1,2,3,4,23,4,5]
print(sumalista(mylista))

def representacion(numero):
    if numero>1:
        return 'A'+ representacion(numero-1)
    else:
        return 'A'

print(representacion(10))


def digitos(numero):
    if numero >9:
        return 1 + digitos(numero//10)
    else:
        return 1


print(digitos(43546234))